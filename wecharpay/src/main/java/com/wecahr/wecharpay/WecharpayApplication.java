package com.wecahr.wecharpay;

import com.wecahr.wecharpay.base.properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication
@Controller
public class WecharpayApplication {


    private static Logger log = LoggerFactory.getLogger(WecharpayApplication.class);


    public static void main(String[] args) {
        SpringApplication.run(WecharpayApplication.class, args);
    }


    @RequestMapping("")
    public void test(){


    }
}
